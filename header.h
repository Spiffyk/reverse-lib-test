#pragma once

/** This does some executable-defined stuff and will be called by the
 * "library", which actually contains the main() function. */
void some_function(void);
