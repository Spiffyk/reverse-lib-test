#include <stdio.h>

#include "header.h"

int main()
{
	printf("Hi, I am the main function from the 'library', calling my_function now:\n");
	some_function();
	return 0;
}
