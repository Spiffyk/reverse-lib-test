# Reverse lib test

I made this tiny test to check out whether I could put a `main()` in a static library, then call a function declared in
a common header from it, with the function being defined in an executable's source file.

Turns out I can, so that opens some ways for a project I've been thinking of...
